import jenkins.model.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*
import hudson.plugins.sshslaves.*;

global_domain = Domain.global()
credentials_store =
  Jenkins.instance.getExtensionList(
    'com.cloudbees.plugins.credentials.SystemCredentialsProvider'
  )[0].getStore()

// Use ''' key ''' if multiline and 'key\nkey\nkey' if one line
private_key = System.getenv('JENKINS_PRIVATE_KEY')

credentials = new BasicSSHUserPrivateKey(
  CredentialsScope.GLOBAL,
  // Credentional ID
  'jenkins_ssh',
  // Username
  'jenkins_ssh', 
  // Private key
  new BasicSSHUserPrivateKey.DirectEntryPrivateKeySource(private_key),
  // Passphrase
  'jenkins_key_passphrase', 
  // Description
  ''
)

credentials_store.addCredentials(global_domain, credentials)
